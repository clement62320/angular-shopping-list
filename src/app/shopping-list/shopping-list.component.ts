import { Component, OnInit } from '@angular/core';
import { ShoppingList } from 'src/interface/shopping-list';
import { User } from 'src/interface/users';
import { ShoppingListService } from 'src/service/shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  shoppingLists: ShoppingList[] = [];

  constructor(
    private shopppingListService: ShoppingListService
  ) {}

  ngOnInit(): void {
    this.getShoppingLists();
  }

  getShoppingLists(): void {
    this.shopppingListService.getShoppingLists().subscribe(shoppingLists => this.shoppingLists = shoppingLists)
  }

}
