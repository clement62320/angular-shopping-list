import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingList } from 'src/interface/shopping-list';
import { ShoppingListService } from 'src/service/shopping-list.service';
import { Location } from '@angular/common';
import { User } from 'src/interface/users';
import { UsersService } from 'src/service/users.service';

@Component({
  selector: 'app-shopping-list-detail',
  templateUrl: './shopping-list-detail.component.html',
  styleUrls: ['./shopping-list-detail.component.css']
})
export class ShoppingListDetailComponent implements OnInit {
  @Input() shoppingList?: ShoppingList;
  users:User[] = [];

  constructor(  private route: ActivatedRoute,
    private shoppingListService: ShoppingListService,
    private userService: UsersService,
    private location: Location) { }

  ngOnInit(): void {
    this.getShoppingList();
    this.getUsers()
  }

  getShoppingList(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id') ?? "0")
    this.shoppingListService.getShoppingList(id).subscribe(shoppingList => {
      this.shoppingList = shoppingList
    });
  }

  goBack(): void {
    this.location.back();
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => {this.users = users; console.log(this.users)})
    
  }

  onSelect(user: User): void {
    console.log("     PASSSS")
    console.log(user);
  }
}
