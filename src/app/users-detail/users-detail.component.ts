import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/interface/users';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UsersService } from 'src/service/users.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {
  @Input() user?: User;

  constructor(  private route: ActivatedRoute,
    private userService: UsersService,
    private location: Location) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id') ?? "0")
    this.userService.getUser(id).subscribe(user => {
      this.user = user
    });
  }

  goBack(): void {
    this.location.back();
  }
}
