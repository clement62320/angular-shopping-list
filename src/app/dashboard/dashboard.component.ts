import { Component, OnInit } from '@angular/core';
import { ShoppingList } from 'src/interface/shopping-list';
import { User } from 'src/interface/users';
import { ShoppingListService } from 'src/service/shopping-list.service';
import { UsersService } from 'src/service/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  users : User[] = [] ;
  shoppingLists: ShoppingList[] = [];

  constructor(
    private userService: UsersService,
    private shopppingListService: ShoppingListService
  ) {}

  ngOnInit(): void {
    this.getUsers();
    this.getShoppingLists();
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => this.users = users)
  }

  getShoppingLists(): void {
    this.shopppingListService.getShoppingLists().subscribe(shoppingLists => this.shoppingLists = shoppingLists)
  }
}
