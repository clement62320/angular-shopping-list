import { Component, OnInit } from '@angular/core';
import { User } from 'src/interface/users';
import { UsersService } from 'src/service/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users : User[] = [] ;
  
  constructor(private userService: UsersService) {}

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => this.users = users)
  }

}
