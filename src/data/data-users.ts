import { User } from "src/interface/users";

export const USERS: User[] = [
  { id: 11, name: 'Clément' },
  { id: 12, name: 'Perrine' },
  { id: 13, name: 'Perle' },
  { id: 14, name: 'User 1' },
  { id: 15, name: 'User 2' },
];