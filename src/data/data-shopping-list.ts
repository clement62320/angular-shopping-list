import { ShoppingList } from "src/interface/shopping-list";
import { USERS } from "./data-users";

export const SHOPPINGLIST: ShoppingList[] = [
    {id:11, name:"Liste de Course 1", user:USERS[0]},
    {id:12, name:"Liste de Course 2", user:USERS[1]},
    {id:13, name:"Liste de Course 3", user:USERS[2]},
    {id:14, name:"Liste de Course 4", user:USERS[3]},
    {id:15, name:"Liste de Course 5", user:USERS[4]}
]