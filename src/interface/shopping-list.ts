import { User } from "./users";

export interface ShoppingList{
    id: number,
    name: string,
    user: User,
}