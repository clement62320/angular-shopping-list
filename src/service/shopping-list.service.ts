import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SHOPPINGLIST } from 'src/data/data-shopping-list';
import { ShoppingList } from 'src/interface/shopping-list';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  constructor() { }

  getShoppingLists(): Observable<ShoppingList[]> {
    return of(SHOPPINGLIST);
  }

  getShoppingList(id: number): Observable<ShoppingList | undefined> {
    return of(SHOPPINGLIST.find(shoppingList => shoppingList.id === id));
  }
}
