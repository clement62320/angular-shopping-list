import { Injectable } from '@angular/core';
import { USERS } from 'src/data/data-users';
import { User } from 'src/interface/users';

import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'any'
})
export class UsersService {

  constructor() { }

  getUsers(): Observable<User[]> {
    return of(USERS);
  }

  getUser(id: number): Observable<User | undefined> {
    return of(USERS.find(user => user.id === id));
  }
}
